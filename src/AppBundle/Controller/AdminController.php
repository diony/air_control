<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use EasyCorp\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;
use Symfony\Component\Validator\Constraints\Null;
use Doctrine\Common\Cache\ArrayCache;

//use SysBackenBundle\EventListener\EasyAdminSubscriber;
use Symfony\Component\EventDispatcher\EventDispatcher;


class AdminController extends BaseAdminController
{

    /**
     * QueryBuilder in List and Search actions.
     *
     * @var QueryBuilder The Query Builder instance
     */
    private $queryBuilder;


    /*public function __construct(){

        $repositoryInspectorRequest = $this->getDoctrine()
            ->getRepository('SysBackenBundle:RequestInspector' );

        $dispatcher = new EventDispatcher();
        $subscriber = new EasyAdminSubscriber($em);
        $dispatcher->addSubscriber($subscriber);
    }*/

    /**
     * The method that is executed when the user performs a 'list' action on an entity.
     *
     *
     * @return Response
     */
    protected function listAction()
    {
        $this->get('session')->remove('dashoboard-filter');

        $this->dispatch(EasyAdminEvents::PRE_LIST);

        $fields = $this->entity['list']['fields'];
        $paginator = $this->findAll($this->entity['class'], $this->request->query->get('page', 1), $this->config['list']['max_results'], $this->request->query->get('sortField'), $this->request->query->get('sortDirection'));

        $this->dispatch(EasyAdminEvents::POST_LIST, array('paginator' => $paginator));

        $viewData = $this->viewData('list');

        return $this->render($this->entity['templates']['list'], array(
            'paginator' => $paginator,
            'fields' => $fields,
            'delete_form_template' => $this->createDeleteForm($this->entity['name'], '__id__')->createView(),
            'viewData' => $viewData
        ));
    }

    /*protected function showAction()
    {
        $this->get('session')->remove('dashoboard-filter');

        $this->dispatch(EasyAdminEvents::PRE_SHOW);

        $fields = $this->entity['show']['fields'];
        $paginator = $this->findAll($this->entity['class'], $this->request->query->get('page', 1), $this->config['list']['max_results'], $this->request->query->get('sortField'), $this->request->query->get('sortDirection'));

        $this->dispatch(EasyAdminEvents::POST_SHOW, array('paginator' => $paginator));

        $viewData = $this->viewData('show');

        return $this->render($this->entity['templates']['show'], array(
            'fields' => $fields,
            'viewData' => $viewData
        ));
    }*/




    /**
     * The method that is executed when the user performs a query on an entity.
     *
     * @return Response
     */
    protected function searchAction()
    {

        $this->dispatch(EasyAdminEvents::PRE_SEARCH);

        $searchableFields = $this->entity['search']['fields'];
//        print_r($this->request->query); die();
        $paginator = $this->findBy($this->entity['class'], $this->request->query->get('query'), $searchableFields, $this->request->query->get('page', 1), $this->config['list']['max_results'], $this->request->query->get('sortField'), $this->request->query->get('sortDirection'));
        $fields = $this->entity['list']['fields'];
//        print_r($paginator); die();
        $this->dispatch(EasyAdminEvents::POST_SEARCH, array(
            'fields' => $fields,
            'paginator' => $paginator,
        ));

        $viewData = $this->viewData('list');

        return $this->render($this->entity['templates']['list'], array(
            'paginator' => $paginator,
            'fields' => $fields,
            'delete_form_template' => $this->createDeleteForm($this->entity['name'], '__id__')->createView(),
            'viewData' => $viewData
        ));
    }

    /**
     * Creates Query Builder instance for all the records.
     *
     * @param string      $entityClass
     * @param string      $sortDirection
     * @param string|null $sortField
     *
     * @return QueryBuilder The Query Builder instance
     */
//    protected function createListQueryBuilder($entityClass, $sortDirection, $sortField = null)
//    {
//        $this->queryBuilder = $this->get('app.search.query_builder')->createListQueryBuilder($this->entity, $sortField, $sortDirection);
//
//        /*var_dump($this->queryBuilder);
//        exit;*/
//
//        return $this->queryBuilder;
//    }

    /**
     * Creates Query Builder instance for search query.
     *
     * @param string      $entityClass
     * @param string      $searchQuery
     * @param array       $searchableFields
     * @param string|null $sortField
     * @param string|null $sortDirection
     *
     * @return QueryBuilder The Query Builder instance
     */
//    protected function createSearchQueryBuilder($entityClass, $searchQuery, array $searchableFields, $sortField = null, $sortDirection = null)
//    {
//        $filtersSave = $this->get('session')->get('dashoboard-filter');
//        $filtersSave = ($filtersSave !== null) ? $filtersSave : array();
//
//        $filtersQuery = $this->request->query->get('dasboard-filter');
//        $filtersQuery = ($filtersQuery !== null) ? $filtersQuery : array();
//
//        $filters = array_replace_recursive($filtersSave, $filtersQuery);
//
//        if($filters != null && count($filters) !== 0){
//            $this->get('session')->set('dashoboard-filter', $filters);
//        }
//
//        $this->queryBuilder = $this->get('app.search.query_builder')->createSearchQueryBuilder($this->entity, $searchQuery, $filters, $sortField, $sortDirection);
//
//        return $this->queryBuilder;
//    }

    public function preUpdateEntity($entity)
    {
        if (method_exists($entity, 'setUpdatedAt')) {
            $entity->setUpdatedAt(new \DateTime());
        }
    }

    public function getQueryBuilder(){
        return $this->queryBuilder;
    }

    /**
     * For inject data into view. For this, may be body in the chield control
     *
     * @param $view.  View render (list, show, search, new, edit)
     * @return array
     */
    protected function viewData($view = 'show'){

        return array();
    }


}
