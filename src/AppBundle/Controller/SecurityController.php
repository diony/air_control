<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\UserBundle\Controller\SecurityController as BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request as Request;
use Symfony\Component\Serializer\Tests\Model;

class SecurityController extends BaseController
{
    public function loginAction(Request $request){
	  	//echo'<pre>'; print_r($request); die;
        return parent::loginAction($request);
    }

    protected function renderLogin(array $data)
    {
        if($this->get('session')->get('inspector_login_error')){
            $data['error_login'] = 'The user doesn\'t have an assigned project';
        }
        return $this->container->get('templating')->renderResponse('AppBundle:security:login.html.twig', $data);
    }
}
