<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {

	  $roles = $this->get('security.token_storage' ) ->getToken() ->getRoles();
	  //echo'<pre>'; print_r($roles); die;
	  if($roles[0]->getRole()=="ROLE_SUPER_ADMIN"){
		return $this->redirectToRoute('easyadmin', array('entity' => 'user', 'action' => 'list',  'menuIndex' => 11, 'submenuIndex' => 0));

	  }else{
		return $this->render('@App/theme/home.html.twig');
	  }


    }
}
