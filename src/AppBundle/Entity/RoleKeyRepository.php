<?php
/**
 * Created by PhpStorm.
 * User: roster
 * Date: 6/23/16
 * Time: 5:00 PM
 */

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;


class RoleKeyRepository extends \Doctrine\ORM\EntityRepository
{
    public function findAll()
    {
        return $this->findBy(array(), array('role'));
    }

    function findRolesQueryBuilder(User $user){

        //echo'<pre>'; print_r($user->getRoles()); die;

        if(in_array('ROLE_ADMIN_IASO',$user->getRoles())){

            $statusKey = "ROLE_OPERATOR";
            $QueryBuilder = $this->createQueryBuilder('ts')
                ->select('ts')
                ->where('ts.role_key = :statusKey')
                ->setParameter('statusKey', $statusKey);

        }else{
            $QueryBuilder = $this->createQueryBuilder('ts')
                ->select('ts');
        }

        return $QueryBuilder;


    }

}