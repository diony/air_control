<?php
/**
 * Created by PhpStorm.
 * User: roster
 * Date: 6/23/16
 * Time: 5:00 PM
 */

namespace AppBundle\Entity;

use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\EntityRepository;


class UserRepository extends EntityRepository implements UserLoaderInterface
{
    public function loadUserByUsername($username)
    {
        return $this->createQueryBuilder('u')
            ->where('u.username = :username OR u.email = :email')
            ->setParameter('username', $username)
            ->setParameter('email', $username)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findUsersQueryBuilder($userrol)
    {
        return $this->createQueryBuilder('u')
            ->where('u.roleKey = :userrol')
            ->setParameter('userrol', $userrol);
    }

}