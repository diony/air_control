<?php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Entity\UserRepository")
 * @ORM\Table(name="[user]")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    /**
     * @var string
     *
     * @ORM\Column(name="full_name", type="string", length=255, nullable=true)
     */
    protected $fullName;

  /**
   * @var string
   *
   * @ORM\Column(name="lastName", type="string", length=255, nullable=true)
   */
  protected $lastName;

  /**
   * @var string
   *
   * @ORM\Column(name="FirstName", type="string", length=255, nullable=true)
   */
  protected $firstName;

    /**
     * The user who made the purchase.
     *
     * @var Type circular
     * @ORM\ManyToOne(targetEntity="RoleKey")
     * @ORM\JoinColumn(name="role_key", referencedColumnName="id", nullable=false)
     */
    protected $roleKey;

    /**
     * Set fullName
     *
     * @param string $fullName
     * @return User
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * Get fullName
     *
     * @return string 
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * Set rolesKey
     *
     * @param \AppBundle\Entity\RoleKey $roleKey
     *
     * @return User
     */
    public function setRoleKey(\AppBundle\Entity\RoleKey $roleKey)
    {
        $this->roleKey = $roleKey;

        return $this;
    }

    /**
     * Get rolesKey
     *
     * @return \AppBundle\Entity\RoleKey
     */
    public function getRoleKey()
    {
        return $this->roleKey;
    }

    public function removeAllRoles(){
        $this->roles = array();
    }


    public function __toString(){
        return $this->fullName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }
}
