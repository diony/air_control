<?php
/**
 * Created by PhpStorm.
 * User: roster
 * Date: 7/6/16
 * Time: 11:01 AM
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Entity\RoleKeyRepository")
 * @ORM\Table(name="[role]")
 */
class RoleKey {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", length=32)
     */
    protected $role;

    /**
     * @var string
     *
     * @ORM\Column(name="role_key", type="string", length=32)
     */
    protected $role_key;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set role
     *
     * @param string $role
     *
     * @return RoleKey
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set roleKey
     *
     * @param string $roleKey
     *
     * @return RoleKey
     */
    public function setRoleKey($roleKey)
    {
        $this->role_key = $roleKey;

        return $this;
    }

    /**
     * Get roleKey
     *
     * @return string
     */
    public function getRoleKey()
    {
        return $this->role_key;
    }

    public function __toString(){

        //$s = $this->tokenStorage()->getToken()->getUser();
        //echo'<pre>'; print_r($s); die;
        return $this->role;
    }
}
