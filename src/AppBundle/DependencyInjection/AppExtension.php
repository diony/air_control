<?php

namespace AppBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\Yaml\Parser;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class AppExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    private $container;

    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $this->container = $container;

//        $yaml = new Parser();
//        $ymlPath = $container->getParameter('kernel.root_dir') . '/config/config/const.yml';
//        $appConst = $yaml->parse(file_get_contents($ymlPath));
//        $this->readlevel($appConst, "");
//
//        $ymlPathM = $container->getParameter('kernel.root_dir') . '/config/config/messages.yml';
//        $appConstM = $yaml->parse(file_get_contents($ymlPathM));
//        $this->readlevel($appConstM, "");

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        //$this->container->compile();
    }

    private function readlevel($config, $key)
    {
        if(!is_array($config))
        {
            $key = trim($key, '.');
            $this->container->setParameter($key, $config);
        }
        else
            foreach ($config as $k => $value)
                $this->readlevel ($value, "$key.$k");
    }
}
    