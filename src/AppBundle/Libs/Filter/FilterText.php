<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Libs\Filter;

/**
 * Description of Filter
 *
 * @author roster
 */
class FilterText extends Filter
{
    public $type = 'text'; //Filter type ("refer to html" element in view)

    public function __construct($name, $propertyFilter, $id = "", $attributes = "")
    {
        parent::__construct($name, $propertyFilter, $id, $attributes);
        
        return $this;
    }

    public static function create($name, $propertyFilter, $id = "", $attributes = ""){
        return new FilterText($name, $propertyFilter, $id, $attributes);
    }
    

    //Filter Load Data
    public function loadData($container, $arguments = array())
    {
        parent::loadData($container, $arguments);
    }

    public function build($container)
    {
        parent::build($container);
    }
}
