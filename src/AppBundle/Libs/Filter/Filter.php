<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Libs\Filter;
use Symfony\Component\ExpressionLanguage\Node\ArgumentsNode;

/**
 * Description of Filter
 *
 * @author roster
 */
class Filter
{
    public $type = 'hidden'; //Filter type ("refer to html" element in view)

    //Filter HTML Element
    public $id; //HTML Id (string)
    public $name; //HTML Name (string)
    public $label; //HTML Label (string)
    public $attributes; //HTML Attriutes,  Additional attriutes (string)

    //Filter HTML Container
    public $containerClass = "col-md-3";

    //Filter Load Data
    public $loadMethodName = NULL; //Repository Method Name for loadData
    public $loadDataArguments = NULL; //Array of Arguments in loadMethod
    public $loadDataUrl = NULL; //Filter load from URL when come into trigger
    
    //Data Filter
    public $data = NULL; //Data of filter
    public $value = NULL; //Default value to set

    //To Filter Propertie
    public $propertyFilter; //Filter propertie to filter this filter
    public $propertyDataType = 'string'; //Filter propertie to filter data type
    public $showSignFilter = false; //Show Filter Sing sing(in query) to filter propertie
    public $signValue = '='; //Value Filter Sing sing(in query) to filter propertie

    public $access = NULL;


    //Trigger Filters
    public $triggerFilters = NULL; //Array of Filters : Filters trigger by this filter


    public function __construct($name, $propertyFilter, $id = "", $attributes = "")
    {
        $this->name = $name == "" ? $propertyFilter : $name;

        $this->propertyFilter = $propertyFilter;
       
        $this->id = $id == "" ? $name : $id;
        $this->attributes = $attributes;

        return $this;
    }

    public static function create($name, $propertyFilter, $id = "", $attributes = ""){
        return new Filter($name, $propertyFilter, $id, $attributes);
    }

    //To Filter Propertie
    /**
     * @param $propertyDataType
     * @return $this
     */
    public function setPropertyDataType($propertyDataType)
    {
        $this->propertyDataType = $propertyDataType;

        return $this;
    }


    //Filter HTML Element
    /**
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        
        return $this;
    }

    /**
     * @param $label
     * @return $this
     */
    public function setLabel($label)
    {
        $this->label = $label;
        
        return $this;
    }

    /**
     * @param $attr
     * @return $this
     */
    public function addAttributes($attr)
    {
        $this->attributes .= ' ' . $attr;
        
        return $this;
    }

    //Filter Container
    /**
     * @param string $containerClass
     */
    public function setContainerClass($containerClass)
    {
        $this->containerClass = $containerClass;
    }


    //Data Filter
    /**
     * @param $data
     * @return $this
     */
    public function setData($data)
    {
        $this->data = $data;
        
        return $this;
    }

    /**
     * @param $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return null
     */
    public function getValue()
    {
        return $this->value;
    }

    //Filter Load Data
    /**
     * @param mixed $loadMethodName
     */
    public function setLoadMethodName($loadMethodName = null)
    {
        if($loadMethodName == null){
            $loadMethodName = "toFilter";
        }

        $this->loadMethodName = $loadMethodName;

        return $this;
    }

    /**
     * @param FilterArgument or FilterArgument[] $loadDataArguments
     * @param null $loadDataUrl
     * @return $this
     */
    public function setLoadData($loadDataArguments = NULL, $loadDataUrl = NULL)
    {
        if($loadDataArguments != NULL) {
            if (is_array($loadDataArguments)) {
                $this->loadDataArguments = $loadDataArguments;
            } else {
                $this->loadDataArguments[] = $loadDataArguments;
            }
        }


        if($loadDataUrl !== NULL)
        {
            $this->loadDataUrl = $loadDataUrl;
        }

        return $this;
    }

    /**
     * @param $container
     * @param array $arguments
     */
    public function loadData($container, $arguments = array())
    {        
    }

    /**
     * @param $container
     */
    public function build($container)
    {        
    }


    //Trigger Filters
    public function setTriggers($triggerFilters){
        if(is_array($triggerFilters))
        {
            foreach ($triggerFilters as $filter)
                $this->triggerFilters[] = $filter->id;
        }
        else
        {
            $this->triggerFilters[] = $triggerFilters->id;
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAccess()
    {
        return $this->access;
    }

    /**
     * @param mixed $access
     */
    public function setAccess($access)
    {
        if(is_array($access))
        {
            foreach ($access as $rol)
                $this->access[] = $rol;
        }
        else
        {
            $this->access[] = $access;
        }

        return $this;

    }

    /**
     * @param boolean $showSignFilter
     */
    public function setShowSignFilter($showSignFilter)
    {
        $this->showSignFilter = $showSignFilter;

        return $this;
    }

    /**
     * @param string $signValue
     */
    public function setSignValue($signValue)
    {
        $this->signValue = $signValue;

        return $this;
    }
}
