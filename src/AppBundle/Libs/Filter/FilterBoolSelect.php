<?php
/**
 * Created by PhpStorm.
 * User: roster
 * Date: 8/5/16
 * Time: 6:09 AM
 */

namespace AppBundle\Libs\Filter;


class FilterBoolSelect extends FilterSelect{

    public function __construct($name, $propertyFilter, $id = "", $attributes = ""){

        parent::__construct($name, $propertyFilter, false, $id, $attributes);

        return $this;
    }

    //Filter Load Data
    public function loadData($container, $arguments = array())
    {
        $data = array(
            array('option' => 'True', 'value' => 'true'),
            array('option' => 'False', 'value' => 'false'),
        );

        $this->data = $data;

        parent::loadData($container, $arguments);
    }

    public function build($container)
    {
        parent::build($container);
    }




}