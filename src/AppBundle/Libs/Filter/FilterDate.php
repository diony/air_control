<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Libs\Filter;

/**
 * Description of Filter
 *
 * @author roster
 */
class FilterDate extends Filter
{
    public $type = 'date'; //Filter type ("refer to html" element in view)

    //Filter Date JS-HTML
    public $dateformat = 'dd/mm/yyyy'; //Date fotmat
    public $changeMonth = true; //If allow change the month
    public $changeYear = true; //If allow change the year
    public $minDate = NULL;
    public $maxDate = NULL;  
    public $_hideCalendar = false;
         
    public function __construct($name, $propertyFilter, $setCurrentDate = FALSE, $id = "",  $attributes = "")
    {
        parent::__construct($name, $propertyFilter, $id, $attributes);

        if($setCurrentDate)
        {
            $init_date = new \DateTime();
            $init_date = date_format($init_date, 'd/m/Y');
            $this->setValue($init_date);
        }

        return $this;
    }
    
    public function setDateFormat($format)
    {
        $this->dateformat = $format;
        
        return $this;
    }

    public function setMinDate($date){
        $this->minDate = $date;
        
        return $this;
    }
    
    public function setMaxDate($date){
        $this->maxDate = $date;
        
        return $this;
    }
    
    public function hideCalendar(){
        $this->_hideCalendar = true;
        
        return $this;
    }
        
    public function build($container)
    {
        parent::build($container);
    }
}
