<?php
/**
 * Created by PhpStorm.
 * User: roster
 * Date: 8/5/16
 * Time: 6:09 AM
 */

namespace AppBundle\Libs\Filter;


class FilterEntitySelect extends FilterSelect{

    //Entity
    private $entity;

    public function __construct($entity, $propertyFilter, $multiple = FALSE, $name = "", $id = "", $attributes = ""){

        $this->entity = $entity;

        parent::__construct($name, $propertyFilter, $multiple, $id, $attributes);

        return $this;
    }

    //Filter Load Data
    /**
     * @param mixed $loadMethodName
     */
    public function setLoadMethodName($loadMethodName = null)
    {
        if($loadMethodName == null){
            $loadMethodName = "findAll";
        }

        $this->loadMethodName = $loadMethodName;

        return $this;
    }

    public function loadData($container, $arguments = array())
    {
        $data = $container->get('app.manager.filter_entity')->getValues($this->entity, $this->optionField, $this->valueField, $this->loadMethodName, $this->orderBy, $arguments);

        $this->data = $data;

        parent::loadData($container, $arguments);
    }

    public function build($container)
    {
        parent::build($container);
    }




}