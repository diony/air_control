<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Libs\Filter;

/**
 * Description of Filter
 *
 * @author roster
 */
class Search
{   
    public $textSearch;
    public $textButton;
      
    public function __construct($textSearch = '', $textButton = 'Search')
    {
        $this->textSearch = $textSearch;
        $this->textButton = $textButton;
    }      
    
    public static function create($textSearch = '', $textButton = 'Search')
    {
        return new Search($textSearch, $textButton);
    }
}
