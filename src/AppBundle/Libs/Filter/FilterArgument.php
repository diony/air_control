<?php
/**
 * Created by PhpStorm.
 * User: roster
 * Date: 8/9/16
 * Time: 12:36 PM
 */

namespace AppBundle\Libs\Filter;


class FilterArgument {

    public $type = 'fixed'; //Fixed 'fixed' or Filter 'filter'
    public $field = 'id'; //Field argument (field to filter(query) and post)
    public $value = null; //Value of field
    public $dataType = 'string'; //data type (query)

    public function __construct($value = null, $field = 'id', $type = 'fixed'){

        $this->value = $value;
        $this->field = $field;
        $this->type = $type;
    }

    public static function create($value = null, $field = 'id', $type = 'fixed'){
        return new FilterArgument($value, $field, $type);
    }

    /**
     * @param string $dataType
     */
    public function setDataType($dataType)
    {
        $this->dataType = $dataType;
    }
}