<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Libs\Filter;

/**
 * Description of Filter
 *
 * @author roster
 */
class FilterSelect extends Filter
{
    public $type = 'select'; //Filter type ("refer to html" element in view)

    //Filter HTML Element
    public $multiple = FALSE; //Is multiple (boolean)

    //Filter Load Data
    public $optionField = 'name'; //option fiel that represent in LoadData(query) or entity
    public $valueField = 'id'; //option fiel that represent in LoadData(query) or entity
    public $orderBy = NULL; //Order field in LoadData(query)

    public function __construct($name, $propertyFilter, $multiple = FALSE, $id = "", $attributes = "")
    {              
        $this->multiple = $multiple;
        
        parent::__construct($name, $propertyFilter, $id, $attributes);

        return $this;
     }

    public static function create($name, $propertyFilter, $multiple = FALSE, $id = "", $attributes = ""){
        return new FilterSelect($name, $propertyFilter, $multiple = FALSE, $id = "", $attributes = "");
    }
    

    //Filter Load Data
    /**
     * @param string $optionField
     */
    public function setOptionField($optionField)
    {
        if($optionField == null){
            $optionField = 'name';
        }

        $this->optionField = $optionField;

        return $this;
    }

    /**
     * @param string $valueField
     */
    public function setValueField($valueField)
    {
        if($valueField == null){
            $valueField = 'id';
        }

        $this->valueField = $valueField;

        return $this;
    }

    /**
     * @param string $orderBy
     */
    public function setOrderBy($orderBy)
    {
        if($orderBy == null){
            $orderBy = 'name';
        }

        $this->orderBy = $orderBy;

        return $this;
    }

    public function loadData($container, $arguments = array())
    {
        parent::loadData($container, $arguments);
    }

    public function build($container)
    {
        parent::build($container);
    }
}
