<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Libs\Filter;

/**
 * Description of Filter
 *
 * @author roster
 */
class DashBoardFilter
{        
    //Filters
    public $filters = array();
    public $search;
    public $collapse = false;
    private $container;
      
    public function __construct($container, $filters = array(), $search = NULL)
    {
        $this->container = $container;
        
        $this->filters = $filters;
        $this->search = $search;
        
        foreach ($filters as &$filter)
        {
            //Build Filter
            $filter->build($this->container);
            
            if($filter->data === NULL)
            {
                //Load Data Filter
                $arguments = array();
                if($filter->loadDataArguments != NULL && count($filter->loadDataArguments) > 0)
                {
                    foreach ($filter->loadDataArguments as $argument)
                    {
                        switch($argument->type){
                            case 'fixed':
                                $arguments[$argument->field] = $argument->value;
                            break;
                        }

//                        $filterArguments = $this->findFilterById($idFilter);
//                        if($filterArguments->value != NULL && $filterArguments->value != "")
//                        {
//                            $arguments[$idFilter] = $filterArguments->value;
//                        }
                    }
                }

                $filter->loadData($this->container, $arguments);
            }
        }
    }       
    
    public static function create($container, $filters = array(), $search = NULL)
    {
        return new DashBoardFilter($container, $filters, $search);
    }

    /**
     * @param boolean $collapse
     */
    public function setCollapse($collapse)
    {
        $this->collapse = $collapse;
    }
    
    private function findFilterById($idFilter)
    {
        foreach ($this->filters as $filter)
            if($filter->id == $idFilter)
            {    
                return $filter;
            }    
    }
}
