<?php

namespace AppBundle\Twig;

use AppBundle\AppBundle;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Twig_Function_Method;
use Twig_SimpleFilter;
use Symfony\Component\Yaml\Parser;

class AppTwigExtension extends \Twig_Extension
{

    /**
     * @var ContainerInterface
     */
    protected $container;


    /**
     * Constructor
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container = null) {
        $this->container = $container;
    }


    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'app.twig.extension';
    }


    public function getFunctions() {
        return array(
           'isAccesiblePath' => new Twig_Function_Method($this, 'isAccesiblePath'),
           'isAccesActionEntityByRol' => new Twig_Function_Method($this, 'isAccesActionEntityByRol'),
           'isAccesFieldEntityByRol' => new Twig_Function_Method($this, 'isAccesFieldEntityByRol'),
           'constApp' => new Twig_Function_Method($this, 'constApp'),
        );
    }

    public function constApp(){
        $configurations = $this->getAppConfig();

        $const = json_encode($configurations['const']);

        return $const;
    }

    /**
     * Retunr if path is accesible
     *
     * @param $path
     * @return bool
     */
    public function isAccesiblePath($path)
    {
        return $path == '#' || $this->container->get('security.route.access')->isAccesibleRoute($path);
    }


    /**
     * Retunr if path is accesible
     *
     * @param $path
     * @return bool
     */
    public function isAccesActionEntityByRol($config,$action)
    {
        $token = $this->container->get('security.token_storage')->getToken();

        foreach($token->getRoles() as $rolUser) {

            //El super admin tiene acceso a todo
            if($rolUser->getRole() == 'ROLE_SUPER_ADMIN'){
                return true;
            }

            if(isset($config['roles_access']['actions'][$action])){
                foreach($config['roles_access']['actions'][$action] as $rolAction){
                    if($rolUser->getRole() == $rolAction){
                        return true;
                    }
                }
            }

        }
        return false;
    }

    /**
     * Retunr if path is accesible
     *
     * @param $path
     * @return bool
     */
    public function isAccesFieldEntityByRol($config)
    {

        $token = $this->container->get('security.token_storage')->getToken();

        if(!isset($config['roles_access'])){
            return true;
        }else {
            foreach ($token->getRoles() as $rolUser) {

                //El super admin tiene acceso a todo
                if ($rolUser->getRole() == 'ROLE_SUPER_ADMIN') {
                    return true;
                }

                foreach ($config['roles_access'] as $rolAction) {
                    if ($rolUser->getRole() == $rolAction) {
                        return true;
                    }
                }


            }
        }
        return false;
    }



    private function getAppConfig()
    {
        $container = $this->container;

        $yaml = new Parser();
        $yml_path = $container->getParameter('kernel.root_dir') . '/config/config/const.yml';

        return $yaml->parse(file_get_contents($yml_path));
    }
}
