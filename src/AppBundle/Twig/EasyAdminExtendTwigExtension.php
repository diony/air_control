<?php

namespace AppBundle\Twig;

use AppBundle\AppBundle;
use JavierEguiluz\Bundle\EasyAdminBundle\EasyAdminBundle;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Twig_Function_Method;
use Twig_SimpleFilter;

use Doctrine\ORM\Mapping\ClassMetadata;
//use JavierEguiluz\Bundle\EasyAdminBundle\Configuration\ConfigManager;
use EasyCorp\Bundle\EasyAdminBundle\Configuration\ConfigManager;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class EasyAdminExtendTwigExtension extends \Twig_Extension
{

    /**
     * @var ContainerInterface
     */
    protected $container;

    private $configManager;
    private $propertyAccessor;
    private $debug;


    
    /**
     * Constructor
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container, $debug = false)
    {

        $this->container = $container;

//        $this->configManager = $configManager;
//        $this->propertyAccessor = $propertyAccessor;
        $this->debug = $debug;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'twig.app.easyadmin.extension';
    }


    public function getFunctions()
    {
        return array(
            'returnPropertyItem' => new Twig_Function_Method($this, 'returnPropertyItem'),
            'returnEasyAdminAccessibleMenu' => new Twig_Function_Method($this, 'returnEasyAdminAccessibleMenu'),
            'dashboardFilters' => new Twig_Function_Method($this, 'dashboardFilters'),
        );
    }

    public function returnPropertyItem($item, $property){

        try {
            $value = $this->propertyAccessor->getValue($item, $property);
        } catch (\Exception $e) {
            throw(new Exception("Inaccesible Property Item"));
        }

        return $value;
    }

    public function dashboardFilters($entityConfig){

        $filters = array();
        $collapse = false;



        $requestFilter = $this->container->get('session')->get('dashoboard-filter');
        $requestFilter = ($requestFilter !== null) ? $requestFilter : array();



        if($filters != null && count($filters) !== 0){
            $this->get('session')->set('dashoboard-filter', $filters);
        }

        if(isset($entityConfig['search']['fields'])) {

            foreach ($entityConfig['search']['fields'] as $pkey => $field) {
//                print_r($field['expose']); die();

//                if(isset($field['expose']) && $this->checkIsAccesibleFilterByRol($field)){
                if(isset($field['expose'])){

                    $fieldExpose = $field['expose'];

                    $filterType = isset($fieldExpose['type']) ? $fieldExpose['type'] : 'entity_select';

                    //To Filter Propertie
                    $propertyFilter = isset($fieldExpose['propertyFilter']) ? $fieldExpose['propertyFilter'] : $field['property'];
                    $propertyDataType = isset($fieldExpose['propertyDataType']) ? $fieldExpose['propertyDataType'] : $field['dataType'];

                    //Filter HTML Element
                    $id = isset($fieldExpose['id']) ? $fieldExpose['id'] : "";
                    $name = isset($fieldExpose['name']) ? $fieldExpose['name'] : $propertyFilter;
                    $label = isset($fieldExpose['label']) ? $fieldExpose['label'] : $name;
                    $attirbutes = isset($fieldExpose['attirbutes']) ? $fieldExpose['attirbutes'] : "";

                    //Filter Container
                    $containerClass =  isset($fieldExpose['class']) ? $fieldExpose['class'] : "col-md-3";

                    //Filter Load Data
                    $loadMethodName = isset($fieldExpose['method']) ? $fieldExpose['method'] : null;
                    $loadDataUrl = isset($fieldExpose['url']) ? $fieldExpose['url'] : null;
                    $loadDataArguments = null;
                    $data = isset($fieldExpose['data']) ? $fieldExpose['data'] : null;
                    $value = isset($fieldExpose['value']) ? $fieldExpose['value'] : null;
                    $showSign = isset($fieldExpose['showSign']) ? $fieldExpose['showSign'] : false;
                    $sign = isset($fieldExpose['sign']) ? $fieldExpose['sign'] : '=';

                    if(isset($fieldExpose['arguments'])){
                        $loadDataArguments = array();

                        foreach($fieldExpose['arguments'] as $akey => $argument){

                            $argumentType = isset($argument['type']) ? $argument['type'] : 'fixed';
                            $argumentField = isset($argument['field']) ? $argument['field'] : 'id';
                            $argumentValue = isset($argument['value']) ? $argument['value'] : null;
                            $argumentDataType = isset($argument['dataType']) ? $argument['dataType'] : 'string';

                            $arg = \AppBundle\Libs\Filter\FilterArgument::create($argumentValue, $argumentField, $argumentType);
                            $arg->setDataType($argumentDataType);
                        }

                        $loadDataArguments[] = $arg;
                    }

                    $filter = null;

                    switch ($filterType){
                        case 'hidden':

                            $filter = new \AppBundle\Libs\Filter\Filter($name, $propertyFilter, $id, $attirbutes);

                            break;

                        case 'text':

                            $filter = new \AppBundle\Libs\Filter\FilterText($name, $propertyFilter, $id, $attirbutes);

                            break;

                        case 'number':

                            //Filter HTML Element
                            $step = isset($fieldExpose['step']) ? $fieldExpose['step'] : 1;

                            $filter = new \AppBundle\Libs\Filter\FilterNumber($name, $propertyFilter, $id, $attirbutes);
                            $filter->setStep($step);

                            break;

                        case 'boolean_select':

                            $filter = new \AppBundle\Libs\Filter\FilterBoolSelect($name, $propertyFilter, $id, $attirbutes);

                            break;

                        case 'entity_select':

                            $entity = $fieldExpose['entity'];

                            //Filter HTML Element
                            $multiple = isset($fieldExpose['multiple']) ? $fieldExpose['multiple'] : false;

                            //Filter Load Data
                            $optionField = isset($fieldExpose['option']) ? $fieldExpose['option'] : null;
                            $valueField = isset($fieldExpose['value']) ? $fieldExpose['value'] : null;
                            $orderBy = isset($fieldExpose['orderBy']) ? $fieldExpose['orderBy'] : $optionField;

                            $filter = new \AppBundle\Libs\Filter\FilterEntitySelect($entity, $propertyFilter, $multiple, $name, $id, $attirbutes);
                            $filter->setOptionField($optionField)
                                ->setValueField($valueField)
                                ->setOrderBy($orderBy);

                            break;

                        case 'date':

                            $dateFormatDefault = 'dd-mm-yyyy';
                            if($this->container->hasParameter('dashboard.date.view.format')){
                                $dateFormatDefault = $this->container->getParameter('dashboard.date.view.format');
                            }

                            //Filter Date JS-HTML
                            $dateformat = isset($fieldExpose['dateformat']) ? $fieldExpose['dateformat'] : $dateFormatDefault;
                            $minDate = isset($fieldExpose['minDate']) ? $fieldExpose['minDate'] : null;
                            $maxDate = isset($fieldExpose['maxDate']) ? $fieldExpose['maxDate'] : null;
                            $setCurrentDate = isset($fieldExpose['setCurrentDate']) ? $fieldExpose['setCurrentDate'] : false;

                            $filter = new \AppBundle\Libs\Filter\FilterDate($name, $propertyFilter, $setCurrentDate, $id, $attirbutes);
                            $value = $value == null ? $filter->getValue() : null;
                            $filter->setDateFormat($dateformat);

                            break;
                    }

                    $filter->setData($data)
                        ->setShowSignFilter($showSign)
                        ->setSignValue($sign)
                        ->setValue($value)
                        ->setLabel($label)
                        ->setPropertyDataType($propertyDataType)
                        ->setLoadMethodName($loadMethodName)
                        ->setLoadData($loadDataArguments, $loadDataUrl)
                        ->setContainerClass($containerClass);

                    if(isset($requestFilter)
                        && is_array($requestFilter)
                        && isset($requestFilter[$name])
                        && $requestFilter[$name]['filter'] != NULL && $requestFilter[$name]['filter'] != ''){

                        $collapse = true;
                        $filter->setValue($requestFilter[$name]['filter']);

                        if(isset($requestFilter[$name]['sign']) && $requestFilter[$name]['sign'] != NULL) {
                            $filter->setSignValue($requestFilter[$name]['sign']);
                        }
                    }

//                    if(isset($field['roles_access'])){
//                        $filter->setAccess($field['roles_access']);
//                    }


                    $filters[] = $filter;
                }
            }
        }


        $dashBoardFilter = new \AppBundle\Libs\Filter\DashBoardFilter($this->container, $filters);
        $dashBoardFilter->setCollapse($collapse);

        return $dashBoardFilter;
    }

    public function returnEasyAdminAccessibleMenu($menu, $menuIndex = 0, $submenuIndex = 0, $hasParent = false){

        foreach($menu as $key => &$menuItem){

            //Get Path
            $path = '';
            switch($menuItem['type']){
                case 'divider':
                    $path =  '#';
                    break;
                case 'link':
                    $path =  $menuItem['url'];
                    break;
                case 'route':
                    $path =  $this->container->get('router')->generate($menuItem['route'], $menuItem['params']);
                    break;
                case 'entity':
                    $path =  $this->container->get('router')->generate('easyadmin', array_merge(
                        array(
                            'entity' => $menuItem['entity'],
                            'action' => 'list',
                        ),
                        array(
                            'menuIndex' => $menuItem['menu_index'],
                            'submenuIndex' => $menuItem['submenu_index']
                        ),
                        $menuItem['params']
                    ));
                    break;
                case 'empty':
                    $path =  '#';
                    break;
            }

            if($this->checkIsAccesiblePath($path)){

                $menuItem['menu_index'] = ($hasParent) ? $menuIndex : $menuIndex++;
                $menuItem['submenu_index'] = ($hasParent) ? $submenuIndex++ : -1;

                if(isset($menuItem['children']) && count($menuItem['children']) > 0){

                    $accessibleChildren = $this->returnEasyAdminAccessibleMenu($menuItem['children'], $menuItem['menu_index'], 0, true);

                    $menuItem['children'] = $accessibleChildren;

                    if(count($menuItem['children']) <= 0){
                        $menuIndex--;
                        unset($menu[$key]);
                    }
                }
            }
            else{

                unset($menu[$key]);
            }

        }

        return $menu;
    }

    /**
     * Retunr if path is accesible
     *
     * @param $path
     * @return bool
     */
    private function checkIsAccesiblePath($path)
    {
        return $path == '#' || $this->container->get('security.route.access')->isAccesibleRoute($path);
    }

    /**
     * @param $roles
     * @return mixed
     */
    private function checkIsAccesibleFilterByRol($field)
    {
        return $this->container->get('security.route.access')->isAccesibleFilterByRol($field);
    }
}
